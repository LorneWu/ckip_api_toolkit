本試用版發行日: 20171107
說明內容版本: 20171107
--------

使用前您需要先進行 路徑設定 
--------
* 設定 Shared library 路徑：
    1) 自行執行環境的設定 
        export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:[您的 CKIPWS_Linux/lib 路徑]	
        export PATH=$PATH:[CKIPWSTester所在目錄]
	# 舉例來說
	# export LD_LIBRARY_PATH=/home/CKIPWS_Linux/lib
	
    2) 或是直接修改系統設定
        1. /etc/ld.so.conf 中新增路徑
            sudo echo /home/CKIPWS_Linux/lib >> /etc/ld.so.conf
        2. 重新載入 shared library 路徑
            sudo ldconfig
* 設定斷詞詞典及統計資料路徑
    編輯 ini/*.ini ，將 Data2 的路徑指向正確的位置

* 可能需要設定語系
    export LC_ALL="zh_TW.UTF-8"


CKIPWS的使用簡介 
--------
單機執行

    CKIPWSTester     ws.ini    test/sample.txt  test/sample.tag    
    執行檔           參數檔    輸入檔　　　     輸出檔
    ws.ini:斷詞設定檔
    test/sample.txt: 輸入檔名
    test/sample.tag: 輸出檔名
    (可參考 demo.sh)


    CKIPWSTesterDir   ws.ini   input      output
    執行檔           參數檔    輸入目錄　 輸出目錄
    (系統會對 input 目錄下的檔案進行斷詞的作, 並輸出到 output 目錄下)    


API執行	
    請參考 CKIPWS.py 的說明
    主要是 指定 inifile, lib, inputStr 的內容，最後呼叫 segment 即可。
    
    
        inifile = 'ws.ini'
        lib = 'libWordSeg.so'
        initial(lib, inifile)
        inputStr = u'蔡英文是中華民國總統,而馬英九為前任總統,新北市市長是朱立倫'
        Result = segment(inputStr, 0)
        print (Result)
    
   (使用方式 python CKIPWS.py )

    first step
        import os

        sys.path.append('[CKIPWSTester所在目錄]')

        import CKIPWS as ckip
   
    second step:
        # 指定 CKIPWS 統系統檔, 請勿修改
        lib = '[您的 CKIPWS_Linux/lib 路徑]libWordSeg.so'
        # 指定 CKIPWS 的設定檔
        inifile = '[CKIPWSTester所在目錄]/ws.ini'
        # 進行 CKIPWS 初始化的動作
        initial(lib, inifile)

        # 輸入要處理的句子
        #inputStr = u'蔡英文是中華民國總統,而馬英九為前任總統,新北市市長是朱立倫'
        #Result = segment(inputStr, 0)
        # 結果在 Result 中
        #print (Result)



# 斷詞系統相關論文
--------
斷詞系統主體
    Chen, K.J. & S.H. Liu, "Word Identification for Mandarin Chinese Sentences," Proceedings of 14th Coling (1992), pp. 101-107.
未知詞處理
    Chen, C. J., M. H. Bai, K. J. Chen, 1997, "Category Guessing for Chinese Unknown Words." Proceedings of the Natural Language Processing Pacific Rim Symposium 1997, pp. 35--40. NLPRS '97 Thailand.
    Chen Keh-Jiann, Ming-Hong Bai, "Unknown Word Detection for Chinese by a Corpus-based Learning Method", International Journal of Computational linguistics and Chinese Language Processing, 1998, Vol.3, #1, pages 27-44.
    Chen Keh-Jiann, Wei-Yun Ma, 2002, "Unknown Word Extraction for Chinese Documents", Proceedings of Coling 2002, pp.169-175.
    Ma, Wei-Yun and Keh-Jiann Chen, 2003, "A Bottom-up Merging Algorithm for Chinese Unknown Word Extraction", Proceedings of ACL, Second SIGHAN Workshop on Chinese Language Processing, pp.31-38.
    Ma, Wei-Yun and Keh-Jiann Chen, 2003, "Introduction to CKIP Chinese Word Segmentation System for the First International Chinese Word Segmentation Bakeoff", Proceedings of ACL, Second SIGHAN Workshop on Chinese Language Processing, pp. 168-171.
詞類標記
    Tsai Yu-Fang and Keh-Jiann Chen, 2003, "Reliable and Cost-Effective Pos-Tagging", Proceedings of ROCLING XV, pp161-174.
    Tsai Yu-Fang and Keh-Jiann Chen, 2003, "Context-rule Model for POS Tagging", Proceedings of PACLIC 17, pp146-151.
    Tsai Yu-Fang and Keh-Jiann Chen, 2004, "Reliable and Cost-Effective Pos-Tagging", International Journal of Computational Linguistics & Chinese Language Processing, Vol. 9 #1, pp83-96.

