# -*- coding: utf-8 -*-
"""
Created on Fri Nov 17 18:28:45 2017

@author: Lorne
"""

import os
import subprocess
import time

def seg(sentence):
    with open('tmp_utf8','w',encoding = 'utf-8') as f:
        f.write(sentence)
    subprocess.run(['CKIPWS\ConvertZ8.02\convertz','/i:utf8','/o:big5','tmp_utf8','tmp_big5'],shell=True)
    if not os.path.isfile('tmp_big5'):
        print(sentence + ": convert failed")
        return []
    os.chdir('CKIPWS')
    subprocess.run(['CKIPWSTester','ws.ini','../tmp_big5','../tmp_result'],shell=True)
    os.chdir('..')
    tmp = ""
    with open('tmp_result','r',encoding='utf-16le') as f:
        tmp = f.read()
    lis = []
    for i in tmp.split('\u3000'):
        tpl = (i[:i.find('(')].replace('\ufeff',''),i[i.find('('):])
        lis.append(tpl)
    os.remove('tmp_utf8')
    os.remove('tmp_big5')
    os.remove('tmp_result')
    return lis
a = seg('我家住在板橋。')
print(a)
