# -*- coding: utf-8 -*-
"""
Created on Fri Nov 10 11:45:23 2017

@author: Lorne

have been finished, But
because it is a test version based on ckip's api,
and it seems like ckip doesn't want to suppurt hole system,
so , it just a little useless.
"""

import time
import socket
from bs4 import BeautifulSoup
from configparser import RawConfigParser
cfg = RawConfigParser()
with open('setting.ini','r') as f:
    cfg.read_file(f,'setting.ini')
host=cfg.get("CKIP_parser_online","host")
port=int(cfg.get("CKIP_parser_online","port"))
account=cfg.get("CKIP_parser_online","account")
password=cfg.get("CKIP_parser_online","password")
import re
def psr_main(s):
    global host
    global port
    global account
    global password
    while True:
        try:
            sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)  
            sock.settimeout(20)
            sock.connect((host,port))
            input_xml = '<?xml version="1.0" ?><wordsegmentation version="0.1"><option showcategory="1" /><authentication username="'+account+'" password="'+password+'" /><text>'+s+'</text></wordsegmentation>'
            sock.sendto(input_xml.encode('big5'),(host,port))
            sp = BeautifulSoup(sock.recv(1024).decode('big5'),"html.parser")
            if sp.find("sentence")!=None:
                return sp.find("sentence").text
            else:
                return sp.find("processstatus").text
        except socket.timeout as e:
            pass
            '''
            except ConnectionResetError:
                print('ConnectionResetError occured , sleep ')
                time.sleep(5)
            '''
        finally:
            sock.close()
def findBracket(s):
    tag = 0
    for i in range(len(s)):
        if s[i] == '(':tag+=1
        elif s[i] == ')' and tag != 1:tag-=1
        elif s[i] == ')' and tag == 1:
            tag-=1
            return i
def sortJson(s):
    tag_key = 0
    i = 0
    dic = []
    while True:
        #input()
        if s[i] == '(' and tag_key!=len(s)-1:
            t = findBracket(s[i:])+i+1
            if i+1<t-1:
                tmpt = sortJson(s[i+1:t-1])
                if tmpt!="" and tmpt!=None and i!=len(s)-1:dic.append((s[tag_key:i],tmpt))
            else:dic.append(s[tag_key:i])
            i = t+1
            tag_key = t+1
        elif s[i] == '|':
            if tag_key == i:continue
            else:
                
                dic.append((s[tag_key:s[tag_key:i].rfind(':')+1+tag_key],s[s[tag_key:i].rfind(':')+1+tag_key:i]))
                tag_key = i+1
        elif i == len(s)-1 and s[i] != ')' and tag_key<i :
            dic.append((s[tag_key:s[tag_key:].rfind(':')+tag_key+1],s[s[tag_key:].rfind(':')+1+tag_key:]))
            break
        else:
            pass
        i +=1
        if i >= len(s):break
    return (tuple(dic) if len(dic)>1 else tuple(dic[0]))

def psr(txt):
    sp = psr_main(txt)
    #print(sp)
    result = (sp[sp.find(' %(')+3:sp.rfind(')')] if sp.find(' %')!=-1 else sp[sp.find(' ')+1:sp.rfind('#')])
    #print(result)
    dic = sortJson(result)
    #print(dic)
    return dic
def psr_article(article):
    seg_mark = "[（）｛｝〔〕【】《》〈〉「」『』，。；：？！…?!:;\"\'\[\]]"
    result = []
    for i in re.split(seg_mark,article):
        if i.strip() != "":
            result.append(psr(i.strip()))
            #time.sleep(3)
    return result

#print(psr('我家住在板橋'))
