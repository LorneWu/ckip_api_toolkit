# -*- coding: utf-8 -*-
"""
Created on Tue Nov 21 20:45:02 2017

@author: Lorne
"""
import socket
from bs4 import BeautifulSoup
from configparser import RawConfigParser
cfg = RawConfigParser()
with open('setting.ini','r') as f:
    cfg.read_file(f,'setting.ini')
host=cfg.get("CKIP_seg_online","host")
port=int(cfg.get("CKIP_seg_online","port"))
account=cfg.get("CKIP_seg_online","account")
password=cfg.get("CKIP_seg_online","password")

def seg(s):
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)  
    global host
    global port
    global account
    global password
    sock.connect((host,port))
    input_xml = '<?xml version="1.0" ?><wordsegmentation version="0.1"><option showcategory="1" /><authentication username="'+account+'" password="'+password+'" /><text>'+s+'</text></wordsegmentation>'
    sock.sendto(input_xml.encode('big5'),(host,port))
    
    
    sp = BeautifulSoup(sock.recv(1024).decode('big5'),"html.parser")
    if sp.find("sentence")!=None:
        return sp.find("sentence").text[1:].split("\u3000")
    else:
        return sp.find("processstatus").text
    sock.close()
print(seg("我家住在板橋"))
