# CKIP api toolkit

這是一個基於 CKIP api學術授權版的工具，必須自行申請帳號密碼。(若是online版需取得socket ip 及 host)。
目前有CKIP parser(online)、CKIP segment(online)及(offline)。
請在學術範圍內合理使用，如有任何侵權行為本人概不負責。

# Getting Start Environment
## CKIP parser 連線版
1. 請取得帳號及密碼，詳細辦法及帳號辦理請至[中研院中文斷詞系統學術授權(線上服務版)](http://parser.iis.sinica.edu.tw/v1/apply.htm)
2. 環境：目前測試環境如下
```
	windows10
	python3
	(python module)
		bs4
		configparser
```
3. 修改setting.ini

## CKIP segment 單機版
1. 請取得CKIPWS的檔案，詳細辦法及核准請至[中研院中文斷詞系統學術授權(下載版)](http://ckipsvr.iis.sinica.edu.tw/ckipws/reg.php)
2. 環境：目前測試環境如下
```
	windows10
	python3
	請將取得的CKIPWS放至ckip_seg_offline.py下，即ckip_seg_offline.py能透過CKIPWS/CKIPWSTester讀取。
```

## CKIP segment 連線版
1. 請取得帳號及密碼，詳細辦法及帳號辦理請至[中文剖析系統線上展示學術授權(線上服務版)](http://ckipsvr.iis.sinica.edu.tw/webservice.htm)
2. 環境：目前測試環境如下
```
	windows10
	python3
	(python module)
		bs4
		configparser
```
3. 修改setting.ini

## New Update : CKIP segment 爬蟲版版 fork from PyCCS
1. CKIP 網頁爬蟲 python3版本
2. 環境：目前測試環境如下
```
	windows10
	python3
```
###*Notice : 錯誤尚未處理*



### About setting.ini
1. 取得CKIP授權後，重新命名setting_example.ini為setting.ini
2. 調整setting.ini內的參數為對應的帳號密碼IP及port
```
[CKIP_seg_online]
host=host
port=port
account=accout
password=password

[CKIP_parser_online]
host=host
port=port
account=accout
password=password
```
>>>>>>> f96aec08ba23fcd944db1ae17d898738755c6742
# How to use
## CKIP parser 連線版
```
from CKIP_parser_online import psr
psr('我家住在板橋')
```
Server Return:
```
#1:1.[0] S(NP(N:我|Head:N:家)|Head:Vt:住|PP(Head:P:在|NP(Head:N:板橋)))#
```
Output:
```
('S', (('NP', (('N:', '我'), ('Head:N:', '家'))), ('Head:Vt:', '住'), ('PP', (('Head:P:', '在'), ('NP', ('Head:N:', '板橋'))))))
```

## CKIP segment 單機版
```
from CKIP_seg_online import seg
seg('我家住在板橋')
```
Server Return:
```
#1:1.[0] S(NP(N:我|Head:N:家)|Head:Vt:住|PP(Head:P:在|NP(Head:N:板橋)))#
```
Output:
```
[('我', '(Nh)'), ('家', '(Nc)'), ('住', '(VCL)'), ('在', '(P)'), ('板橋', '(Nc)'), ('。', '(PERIODCATEGORY)')]
```

## CKIP segment 連線版
```
from CKIP_seg_online import seg
seg('我家住在板橋')
```
Output:
```
['我(N)', '家(N)', '住(Vt)', '在(P)', '板橋(N)']
```

# License
This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details.

